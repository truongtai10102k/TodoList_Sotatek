import * as actionTypes from '../constans';

const createAction = (type) => {
    return {
        request: (payload) => ({ type: `${type.REQUEST}`, payload }),
        success: (payload) => ({ type: `${type.SUCCESS}`, payload }),
        failure: (payload) => ({ type: `${type.FAILURE}`, payload }),
    }
}

export const getItem = createAction(actionTypes.GET_ITEM)
export const postItem = createAction(actionTypes.POST_ITEM)
export const putItem = createAction(actionTypes.PUT_ITEM)
export const deleteItem = createAction(actionTypes.DELETE_ITEM)
export const changeStatus = createAction(actionTypes.CHANGE_STATUS)