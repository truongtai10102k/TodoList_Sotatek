import React from 'react';
import { connect } from 'react-redux';
import FormComponent from '../Component/FormComponent';
import { postItem } from '../action/index';
const FormContainer = (props) => {
    const { postTodo } = props
    return (
        <div style={{
            padding: '0 50px',
          
        }}>
            <FormComponent
                type='add'
                show={true}
                postTodo={postTodo}
            />
        </div>
    );
}

const mapStateToProps = (state) => {
    return {

    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        postTodo: (data) => {
            dispatch(postItem.request(data))
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(FormContainer);