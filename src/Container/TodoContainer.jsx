import React, { useEffect } from 'react';
import TodoComponent from '../Component/TodoComponent';
import { connect } from 'react-redux';
import { getItem, deleteItem, putItem, changeStatus } from '../action/index';
const TodoContainer = (props) => {
    const { getTodo } = props
    useEffect(() => {
        getTodo({ textSearch: '' })
    }, []);
    return (
        <div style={{ padding: '0 50px' }}>
            <TodoComponent {...props} />
        </div>
    );
}


const mapStateToProps = (state) => {
    return {
        listTodo: state.todo.listData
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getTodo: (data) => {
            dispatch(getItem.request(data))
        },
        deleteTodo: (data) => {
            dispatch(deleteItem.request(data))
        },
        putTodo: (data) => {
            dispatch(putItem.request(data))
        },
        changeStatus: (data) => {
            dispatch(changeStatus.request(data))
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(TodoContainer);