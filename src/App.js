import logo from './logo.svg';
import './App.css';
import TodoPage from './Page/TodoPage';
import { createStore, applyMiddleware } from 'redux';
import reduxLogger from 'redux-logger';
import rootReducer from './Reducer/index';
import { Provider } from 'react-redux';
function App() {
  const store = createStore(rootReducer, applyMiddleware(reduxLogger))
  return (
    <div >
      <Provider store={store}>
        <TodoPage />
      </Provider>
    </div>
  );
}

export default App;

