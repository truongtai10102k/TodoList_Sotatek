import React from 'react';
import FormContainer from '../Container/FormContainer';
import TodoContainer from '../Container/TodoContainer';
import '../styles/todoPage.css'
const TodoPage = () => {
    return (
        <div className='page'>
            <div className='add-form'>
                <FormContainer />
            </div>
            <div className='todo-page-container'>
                <TodoContainer />
            </div>
        </div>
    );
}

export default TodoPage;
