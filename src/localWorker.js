
const getData = () => {
    return JSON.parse(localStorage.getItem('Todo'))
}
const setData = (data) => {
    localStorage.setItem('Todo', JSON.stringify(data))
}


export { getData, setData };