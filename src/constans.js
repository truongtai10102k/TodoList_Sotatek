const createActionType = (action) => {
    return {
        REQUEST: action + '_REQUEST',
        SUCCESS: action + '_SUCCESS',
        FAILURE: action + '_FAILURE',
    }
}

export const GET_ITEM = createActionType("GET_ITEM")
export const POST_ITEM = createActionType("POST_ITEM")
export const PUT_ITEM = createActionType("PUT_ITEM")
export const DELETE_ITEM = createActionType("DELETE_ITEM")
export const CHANGE_STATUS = createActionType("CHANGE_STATUS")