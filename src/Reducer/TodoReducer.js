import { getData, setData } from '../localWorker';
import * as types from '../constans';
let DEAFULT_STATE = {
    listData: [],
}

const todoReducer = (state = DEAFULT_STATE, action) => {
    switch (action.type) {
        case types.POST_ITEM.REQUEST:
            let dataCurrent = action.payload;
            dataCurrent.id = Math.ceil(Math.random() * 1000000000)
            dataCurrent.status = false
            dataCurrent.dateNumber = action.payload.date.split("-").join('')
            let dataLocalPost = getData() || [];
            dataLocalPost.push(dataCurrent)

            dataLocalPost.sort((a, b) => a.dateNumber - b.dateNumber)

            setData(dataLocalPost)
            return {
                listData: dataLocalPost
            }

        case types.GET_ITEM.REQUEST:
            let dataLocalGet = getData() || [];
            let listDataGet = []
            if (action.payload.textSearch === '') {
                listDataGet = dataLocalGet
            } else {
                listDataGet = dataLocalGet.filter((item) => item.title.includes(action.payload.textSearch))
            }
            // let sortByDate 
            return {
                listData: listDataGet
            }

        case types.DELETE_ITEM.REQUEST:
            let dataLocalDelete = getData() || [];
            let newTodoArray = []
            action.payload.map((id) => {
                dataLocalDelete = dataLocalDelete.filter((item) => item.id !== id)
            })
            setData(dataLocalDelete)
            return {
                listData: dataLocalDelete
            }
        case types.PUT_ITEM.REQUEST:
            let dataUpdate = action.payload;
            dataUpdate.dateNumber = action.payload.date.split("-").join('')
            let dataLocalPut = getData() || [];
            let indexUpdate = dataLocalPut.findIndex((item) => item.id === action.payload.id)
            dataLocalPut[indexUpdate] = dataUpdate
            dataLocalPut.sort((a, b) => a.dateNumber - b.dateNumber)
            setData(dataLocalPut)
            alert("Update thành công")

            return {
                listData: dataLocalPut
            }

        case types.CHANGE_STATUS.REQUEST:
            let dataLocalStatus = getData() || [];
            const { payload } = action
            payload.map((id) => {
                let indexChange = dataLocalStatus.findIndex((item) => item.id === id)
                dataLocalStatus[indexChange].status = !dataLocalStatus[indexChange].status
            })
            setData(dataLocalStatus)
            alert("Đổi trạng thái thành công")
            return {
                listData: dataLocalStatus
            }

        default:
            return {
                ...state
            }
    }
}
export default todoReducer