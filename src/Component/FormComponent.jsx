import React, { useState, useEffect } from 'react';
import '../styles/FormComponent.css'


const FormComponent = (props) => {
    const { type, show, postTodo, row, putTodo,setShow } = props

    useEffect(() => {
        if (type === 'update') {
            setTitle(row?.title || '')
            setDescription(row?.description || '')
            setDate(row?.date || '')
            setPiority(row?.piority || '')
        }
    }, [row]);

    let currentDate = () => {
        let date = new Date().toLocaleDateString().split('/').reverse()
        if (date[2].length === 1) {
            date[2] = '0' + date[2]
        }
        return date.join("-")
    }

    const [title, setTitle] = useState('');
    const [description, setDescription] = useState('');
    const [date, setDate] = useState(currentDate());
    const [piority, setPiority] = useState('Normal');

    const hanldeSetState = (state, methodSetState) => {
        methodSetState(state)
    }

    const hanldeAddTodo = () => {
        if (title) {
            postTodo({ title, description, date, piority })
        } else {
            alert("Oops title is required")
        }
    }
    const hanldePutTodo = () => {
        if (title) {
            putTodo({ title, description, date, piority, id: row.id })
            setShow(!show)
        } else {
            alert("Oops title is required")
        }
    }

    return (
        <div className={`${show ? 'show' : 'hidden'}`} >
            {
                type === 'add'
                &&
                <h2 className='header '>New Task</h2>
            }
            <input
                className='input block'
                type='text'
                value={title}
                placeholder='Add new task ...'
                onChange={(e) => { hanldeSetState(e.target.value, setTitle) }}
            />
            <div className='block'>
                <div className='title'>Description</div>
                <textarea
                    rows='8'
                    value={description}
                    className='text-area'
                    onChange={(e) => { hanldeSetState(e.target.value, setDescription) }}
                />
            </div>
            <div className='group-box block'>
                <div className='item-group-box'>
                    <div className='title' >Due Date</div>
                    <input
                        className='input-group'
                        type='date'
                        min={currentDate()}
                        value={date}
                        defaultValue={currentDate()}
                        onChange={(e) => { hanldeSetState(e.target.value, setDate) }}
                    />
                </div>
                <div className='item-group-box'>
                    <div className='title'>Piority</div>
                    <select
                        className='input-group'
                        value={piority}
                        onChange={(e) => { hanldeSetState(e.target.value, setPiority) }}
                    >
                        <option value='Low' >Low</option>
                        <option selected value='Normal'>Normal</option>
                        <option value='High'>High</option>
                    </select>
                </div>
            </div>
            <button
                className='button'
                onClick={
                    type === 'add'
                        ?
                        hanldeAddTodo
                        :
                        hanldePutTodo
                }
            >{type === 'add' ? 'Add' : 'Update'}</button>
        </div>
    );
}

export default FormComponent;
