import React, { useState, useEffect } from 'react';
import '../styles/FormComponent.css'
import FormComponent from './FormComponent';
const TodoComponent = (props) => {
    const [listChoose, setListChoose] = useState([]);
    const [textSearch, setTextSearch] = useState('');
    const { deleteTodo, putTodo, listTodo, changeStatus, getTodo } = props

    let listTodoComponent = []
    if (listTodo) {
        listTodoComponent = listTodo.map((item, idx) => {
            return (
                <ItemTodo
                    listChoose={listChoose}
                    setListChoose={setListChoose}
                    key={idx}
                    row={item}
                    deleteTodo={deleteTodo}
                    putTodo={putTodo}
                />
            )
        })

    }

    const hanldeSearch = (e) => {
        if (e.key === "Enter") {
            getTodo({ textSearch })
        }
    }
    return (
        <div >
            <h2 className='header '>To Do List</h2>
            <input
                className='input block'
                type='text'
                placeholder='Search ...'
                debounce
                onChange={
                    (e) => setTextSearch(e.target.value)

                }
                onKeyPress={hanldeSearch}
            />
            {listTodoComponent}
            {
                listChoose.length > 0
                &&


                <div className='footer'>
                    <div>Bulk Action:</div>
                    <div className='todo-button'>
                        <button
                            className='todo-button-done'
                            onClick={() => {
                                changeStatus(listChoose)
                                setListChoose([])
                            }}
                        >Done</button>
                        <button className='todo-button-remove'
                            onClick={() => {
                                deleteTodo(listChoose)
                                setListChoose([])
                            }}
                        >Remove</button>
                    </div>
                </div>
            }
        </div>
    );
}

export default TodoComponent;


const ItemTodo = (props) => {
    const { idx, row, deleteTodo, listChoose,
        setListChoose, putTodo } = props
    const [show, setShow] = useState(false);
    const [checked, setChecked] = useState(false);
    useEffect(() => {
        if (listChoose.length === 0) {
            setChecked(false)
        }
    }, [listChoose]);
    const handleChoose = (e) => {
        let currentChoose = listChoose

        if (e.target.checked) {
            setListChoose([...listChoose, row.id])
            setChecked(true)
        } else {
            let newArr = currentChoose.filter(item => item !== row.id)
            setListChoose(newArr)
            setChecked(false)
        }
    }

    return (
        <div className='todo-container block' key={idx} >
            <div className={`${show ? 'todo-border-bottom todo-group' : 'todo-group'} `}>
                <div className='todo-left'>
                    <input type='checkbox' className='todo-checkbox' checked={checked} onChange={handleChoose} />
                    <div className='todo-content'
                        style={row.status ? { textDecoration: 'line-through' } : { textDecoration: 'none' }}
                    >{row?.title}</div>
                </div>
                <div className='todo-button'>
                    <button
                        className='todo-button-detail'
                        onClick={() => {
                            setShow(!show)
                        }}>Detail</button>
                    <button className='todo-button-remove'
                        onClick={() => {
                            deleteTodo([row.id])
                        }}
                    >Remove</button>
                </div>
            </div>
            <div className={`${show && "todo-detail"}`}>
                <FormComponent setShow={setShow} putTodo={putTodo} type='update' show={show} row={row} />
            </div>
        </div>
    )
}